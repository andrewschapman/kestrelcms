<?php
class RobotsController extends BaseController
{
    public function __construct($di)
    {
        parent::__construct($di);

        $this->model = new Robots();
        $this->model->init($di);
    }
}
