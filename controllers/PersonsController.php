<?php
class PersonsController extends BaseController
{
    public function __construct($di)
    {
        parent::__construct($di);

        $this->model = new Persons();
        $this->model->init($di);
    }
}
