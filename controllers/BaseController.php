<?php
/**
* The Controller abstract class provides the skeleton that all
* controllers should inherit from.  
*/
abstract class BaseController
{
    protected $di;          // An instance of the Phalcon dependency injection object
    protected $model;       // An instance of the primary model for this handler.
    protected $result;      // An return array with STATUS and MESSAGE nodes
    protected $request;       // An instance of the Phalcon HTTP Request object
    protected $response;      // An instance of the Phalcon HTTP Response object
    protected $objURI;        // A reference to the global URI object created in index.php 
    protected $config;        // A reference to the global Config object, created in index.php
        
    private $numRecs;       // Stores the total number of records for calls such as getList.

    public function __construct($di)
    {
        global $objURI;
        global $config;
        
        $this->di = $di;
        //$this->request = new \Phalcon\Http\Request();
        $this->request = $di->getDefault()->getRequest();
        $this->response = $di->getDefault()->getResponse();
        $this->result = array("status" => false, "message" => "An unspecified error occured");
        $this->numRecs = 0;
        $this->objURI = $objURI;
        $this->config = $config;
    }
    
    /**
    * Gets a singular item for the controller's model.
    * and returns the item in JSON format.
    * 
    * @param integer $id The id of the item to load and output the JSON for
    */
    public function get($id)
    {
        if(!is_numeric($id)) {
            return false;
        }

        $item = $this->model->get($id);
        if(!$item) {
            $this->result["message"] = "Invalid ID";
            $this->send();  
        }

        $this->outputAsJson($item);       
    }    

    /**
    * Gets a list of the records from the controller's model and then outputs as json.
    * The $_POST array will be used by the model to achieve any filtering necessary.
    */
    public function getList()
    {
        try {
            // Check for a page parameter
            $page = $this->request->get("page");
            if(!is_numeric($page)) {
                $page = 0;
            }
            
            // Check for order By
            $orderCol = $this->request->get("order_col");
            $orderDir = $this->request->get("order_dir");
            
            // Build the order by string, taking into account what the model allows
            $orderBy = $this->model->getOrderBy($orderCol, $orderDir);
            
            // Get the list result, using the PHP get array as the basis for the filters
            $result = $this->model->getList($_GET, $orderBy, $page, $totalRows);
            $numRows = $result->count();
            
            $data = array();
            if($numRows > 0) {
                $data = $result->toArray();
            }
            
            if($page > 0) {
                $data["totalRows"] = $totalRows;
            }

            $this->outputAsJson($data);  
        } catch(Exception $e) {
            $this->error($e->getMessage());
        }     
    }  
    
    
    /**
    * The base save method does not handle any of the save operation,
    * as the functionality required from case to case is to specific.
    * It does however do any generic preparation work.
    * 
    * @param int $id The id of the item to save
    */
    public function save($id = "")
    {
        try {
            $this->handleJSONContentType();
            
            // The data array is provided by the PHP $_POST array
            $data = $_POST;             
            
            // If an id was passed, do an update.
            if(is_numeric($id)) {
                $this->model = $this->model->get($id);
                if(!$this->model) {
                    throw new Exception("Invalid ID - doesn't exist in database");
                }                                                              
            }         

            // Assign the values to the model
            $this->model->assign($data);           
            
            if(!$this->model->save()) {
                $errorStr = "";
                
                foreach ($this->model->getMessages() as $message) {
                    $errorStr .= $message->getMessage();
                    $errorStr .= ", Field: " . $message->getField();
                    $errorStr .= ", Type: " . $message->getType();
                    $errorStr .= ". \n";
                }
                
                throw new Exception("Save failed: $errorStr");
            }
            
            // If we reach here, the new record was created
            // Send the OK back with the ID in the message
            $this->ok($this->model->id);
            
        } catch(Exception $e) {
            $this->error($e->getMessage());    
        }
    }  
    
    /***
    * Deletes items.  There can be multiple, specified by 
    * separating individual ids with commas.
    * 
    * @param integer or string $ids The id (or ids) to delete.  e.g. 1 or 1,2,3
    */
    public function delete($ids = "")
    {
        if(empty($ids)) {
            $this->result["message"] = "Please specify valid ids to delete";
            $this->send();             
        }
        
        $targets = array();
        if(is_numeric($ids)) {
            $targets[] = $ids;
        } else {
            $targets = explode(",", $ids);
        }
        
        foreach($targets as $target_id) {
            $target_id = intval($target_id);
            $item = $this->model->get($target_id);
            if(!$item) {
                $this->error("Delete failed - an item with an id of $target_id does not exist ");  
            }
            
            if(!$item->delete()) {
                $this->error("The item with an id of $target_id could not be deleted");  
            }
        }

        $this->ok();          
    }

    
    /**
    * Outputs an array as a json encoded response.  
    * Very useful for loading data via AJAX.
    * 
    * @param $data An array of data
    * @param boolean $exit Set to true (default) to exit PHP processing after sending result.
    */
    protected function outputAsJson($data, $exit = true)
    { 
        $this->ok($data);
    }
    
    /**
    * Outputs a single bean as a json encoded response. 
    * The result will be a JSON encoded single bean (NOT an array)
    * 
    * @param RedBeanResult $beans
    * @param boolean $exit Set to true (default) to exit PHP processing after sending result.
    */
    protected function outputBeanAsJson($bean, $exit = true)
    {
        // Convert all beans to an array
        $beanArray = R::exportAll($bean);
        
        // If there are no beans, just output a blank message.
        if(count($beanArray) == 0) {
            $this->ok("");
        }
        
        $this->ok($beanArray[0]);
    }    
    
    /**
    * Outputs the class result array as a json message
    * Used extensively for AJAX communications
    * 
    * @param boolean $exit Set to false if the app should NOT exit after the message is sent.
    */
    protected function send($exit = true)
    {
        $this->response->setHeader("Content-Type", "application/json");
        
        echo json_encode($this->result);
        
        if($exit) {
            exit();
        }        
    }
    
    /**
    * Sends a JSON encoded "OK" result to the client
    */
    protected function ok($message = "")
    {
        $this->result["status"] = true;
        $this->result["message"] = $message;
        
        /*
        if($this->numBeans > 0) {
            $this->result["pages"] = floor($this->numBeans / MYNDIE_ITEMS_PER_PAGE);
            if(($this->numBeans % MYNDIE_ITEMS_PER_PAGE) != 0) {
                $this->result["pages"]++;    
            }
        } 
        */       
        
        $this->send();
    }
    

    /**
    * Sends a JSON encoded "ERROR" result to the client
    *     
    * @param string $message The error message to send
    */
    protected function error($message)
    {
        $this->result["status"] = false;
        $this->result["message"] = $message;
        $this->send();        
    }
    
    /**
    * The Angular JS framework oftens sends data to the server in JSON format, rather than
    * standard post vars.  This method detects JSON encoding and converts the variables back to post.
    */
    protected function handleJSONContentType()
    {
        // Detect a JSON submission and convert to $_POST
        $content_type = $this->request->getHeader("Content-Type") ;
        if(stristr($content_type, "application/json")) {
            $_POST = (array) json_decode(file_get_contents("php://input"));
        }        
    }
}
