<?php
use Phalcon\Events\EventsAwareInterface;

class UserController extends BaseController
{
    public function __construct($di)
    {
        parent::__construct($di);

        $this->model = new User();
        $this->model->init($di, $this->config);
    }     
    
    public function login()
    {
        try {
            $this->handleJSONContentType();
            
            $email = $this->request->getPost("email");
            $password = $this->request->getPost("password");
            
            // Ensure both email and password have been provided
            if((empty($email)) || (empty($password))) {
                throw new Exception("You must provide both the email and password fields in order to login");
            }
            
            //  Ensure the email address is a valid email address
            if(!filter_var($email, FILTER_VALIDATE_EMAIL)) {
                throw new Exception("The email address is invalid");    
            }
            
            // Check for an existing user account
            $objUser = $this->model->getByEmail($email);
            if(!$objUser) {
                throw new Exception("Sorry your login failed, please check your email address and password.  Reason 1.");    
            }
            
            // Set the model instance to the loaded user object
            $this->model = $objUser;
            
            // Ensure the passed password matches the user account password.
            if(!$this->model->checkPassword($password)) {
                throw new Exception("Sorry your login failed, please check your email address and password.  Reason 2.");    
            }
            
            // Create a new session
            Kestrel\Session::createSession($this->config);
            
            // The session was created OK - send the OK status back
            // with the sessionID in the payload so the client can pass
            // it back as needed
            $this->ok(Kestrel\Session::getSessionID());            
        } catch(Exception $e) {
            $this->error($e->getMessage());    
        }        
    }
    
    /**
    * The register method allows a new user account to be created.
    * To update an existing account, the standard save method should 
    * be used instead.
    * 
    * @param int $id The id of the item to save
    */
    public function register()
    {
        try {
            $this->handleJSONContentType();
            
            // The data array is provided by the PHP $_POST array
            $data = $_POST; 
            
            // Assign the values to the model
            $this->model->assign($data); 
            
            // Call the validation method early
            if(!$this->model->validation()) {
                $errorStr = "";
                
                foreach ($this->model->getMessages() as $message) {
                    $errorStr .= $message->getMessage();
                    $errorStr .= ", Field: " . $message->getField();
                    $errorStr .= ", Type: " . $message->getType();
                    $errorStr .= ". \n";
                }
                
                throw new Exception("Save failed: $errorStr");                
            }
            
            // Hash the password
            $password_clear = $this->request->getPost("password");
            $password = "";
            $salt = "";
            
            // The hashPassword method will set the $password and $salt params
            // These are reference parameters
            $this->model->hashPassword($password_clear, $password, $salt);
            $this->model->password = $password;
            $this->model->salt = $salt;            
            
            if(!$this->model->save()) {
                $errorStr = "";
                
                foreach ($this->model->getMessages() as $message) {
                    $errorStr .= $message->getMessage();
                    $errorStr .= ", Field: " . $message->getField();
                    $errorStr .= ", Type: " . $message->getType();
                    $errorStr .= ". \n";
                }
                
                throw new Exception("Save failed: $errorStr");
            }
            
            // If we reach here, the new record was created.            
            // Send the registration welcome email.
            $emailTemplate = Emailtemplate::findFirst("code = 'new_user_registered'");
            if(!$emailTemplate) {
                throw new Exception("Invalid email template");
            }
            
            // Populate email data array
            $emailData = array();
            $emailData["first_name"] = $this->model->first_name;
            $emailData["last_name"] = $this->model->last_name;
            
            // Define recipients
            $recipients = array(
                $this->model->first_name . " " . $this->model->last_name => $this->model->email
            );                   
            
            $emailTemplate->sendEmail($emailData, $recipients, false, false);            
            
            // Send the OK back with the ID in the message
            $this->ok($this->model->id);
            
        } catch(Exception $e) {
            $this->error($e->getMessage());    
        }
    }    
}
