<?php
use Phalcon\Mvc\Model\Validator\InclusionIn,
    Phalcon\Mvc\Model\Validator\PresenceOf,
    Phalcon\Mvc\Model\Message,
    Phalcon\Mvc\Model\Validator\Uniqueness;
    
class Mechanics extends \Phalcon\Mvc\Model
{
    // Include any traits this model implements
    use StandardModel;
    
    public $name;
    public $phone;
    
    public function init($di)
    {
        parent::init($di);
        
        $this->defaultOrderBy = "name ASC";
        $this->itemsPerPage = 10;
    }
    
    public function validation()
    {
        // Mechanic name must be unique
        $this->validate(new Uniqueness(
            array(
                "field"   => "name",
                "message" => "The mechanic name must be unique"
            )
        ));

        //Check if any messages have been produced
        if ($this->validationHasFailed() == true) {
            return false;
        }
    }
    
    protected function applyFilters($filters, &$conditions = array(), &$bind = array()) 
    {               
        $this->addFilter("name", $filters, $conditions, $bind, "LIKE");     
    }   
}