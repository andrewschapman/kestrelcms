<?php
use Phalcon\Mvc\Model\Validator\InclusionIn,
    Phalcon\Mvc\Model\Validator\PresenceOf,
    Phalcon\Mvc\Model\Message,
    Phalcon\Mvc\Model\Validator\Uniqueness,
    Phalcon\Mvc\Model\Validator\Email as Email;

class User extends \Phalcon\Mvc\Model
{
    // Include any traits this model implements
    use StandardModel;

    /**
     *
     * @var string
     */
    public $first_name;

    /**
     *
     * @var string
     */
    public $last_name;

    /**
     *
     * @var string
     */
    public $email;

    /**
     *
     * @var string
     */
    public $password;

    /**
     *
     * @var string
     */
    public $salt;
    
    /**
    * Finds the user in the database, using the email address as the key
    * @param email $email
    */
    public function getByEmail($email)
    {
        $item = $this->findFirst("email='" . $email . "'");
        return $item;    
    } 
    
    
    /**
    * Evaluates whether the password passed matches the users password or not.
    * 
    * @param string $password The password to test.
    * @returns True on success, false on failure.
    */
    public function checkPassword($password)
    {
        // If we're using PHP's built in password hashing system, use the password_verify to test.
        if($this->config->application->hashMode == "BCRYPT") {
            if(!password_verify($password, $this->password)) {
                return false;
            }
        } else {
            // Use the SHA256 hashing method.
            // Does the password match
            $salt = $this->salt;
            $hashedPassword = "";
            
            $this->hashPassword($password, $hashedPassword, $salt);
            
            if($user->password != $hashedPassword) {
                return false;
            }
        } 
        
        return true;       
    }   


    /**
     * Validations and business logic
     */
    public function validation()
    {
        $this->validate(new PresenceOf(array(
            'field' => 'first_name',
            'message' => 'The first_name field is required'
        ))); 
        
        $this->validate(new PresenceOf(array(
            'field' => 'last_name',
            'message' => 'The last_name field is required'
        )));  
        
        $this->validate(new PresenceOf(array(
            'field' => 'email',
            'message' => 'The email field is required'
        ))); 
        
        $this->validate(new PresenceOf(array(
            'field' => 'password',
            'message' => 'The password field is required'
        )));                       
        
        $this->validate(new Email(array(
            'field'    => 'email',
            'required' => true
        )));
        
        $this->validate(new Uniqueness(
            array(
                "field"   => "email",
                "message" => "This email address has already been used by another user."
            )
        ));        
        
        if ($this->validationHasFailed() == true) {
            return false;
        }
        
        return true;
    }

    /**
     * Independent Column Mapping.
     */
    public function columnMap()
    {
        return array(
            'id' => 'id', 
            'first_name' => 'first_name', 
            'last_name' => 'last_name', 
            'email' => 'email', 
            'password' => 'password', 
            'salt' => 'salt', 
            'modified_dtm' => 'modified_dtm', 
            'created_dtm' => 'created_dtm'
        );
    }
    
    /**
    * Init method is used to define legal ordering columns, 
    * order-by behaviour, pagination limits et cetera.
    * 
    * @param mixed $di
    */
    public function init($di, $config)
    {
        $this->allowedOrderColumns = array("User.id", "User.first_name", "User.last_name", "User.created_dtm");
        $this->defaultOrderBy = "User.first_name ASC";
        $this->itemsPerPage = 25;    
        $this->config = $config;    
    }
    
    /**
    * The getList method, as defined by the standard model trait, will invoke this 
    * apply filters method. Here we can define which columns are searchable, and what 
    * the search condition is. For example =, LIKE, <, >, >= etc
    * 
    * @param array $filters The filters array
    * @param array $conditions The conditions array that is populated by this method.
    * @param mixed $bind
    */
    protected function applyFilters($filters, &$conditions = array(), &$bind = array()) 
    {               
        $this->addFilter("first_name", $filters, $conditions, $bind, "LIKE");
        $this->addFilter("last_name", $filters, $conditions, $bind, "LIKE");     
        $this->addFilter("email", $filters, $conditions, $bind, "LIKE");
        $this->addFilter("created_dtm", $filters, $conditions, $bind, ">=");
    } 
    
    /***
    * Hashes the specific input password, and generates a resultant hash.
    * Note the hash will either be BCRYPT or SHA256, depending on the 
    * MYNDIE_HASH_MODE setting in the contants file.
    * 
    * @param string $input The plain text password to hash
    * @param string $password An output param - the resultant hash
    * @param string $salt An output param - the resultant salt (note: no salt will be generated if BCRYPT is used)
    */
    public function hashPassword($input, &$password, &$salt = "")
    {
        if(empty($input)) {
            return false;
        }
        
        if($this->config->application->hashMode == "BCRYPT") {
            $password = password_hash($input, PASSWORD_BCRYPT);
            $salt = "";
            return true;
        }
        
        // If no salt value has been passed, create one
        if(empty($salt)) {
            $salt = Strings::createRandomString();
        }
        
        // Hash the password using the salt.
        $password = hash("SHA256", $input . $salt);

        return true;
    }            
}
