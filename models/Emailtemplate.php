<?php
use Phalcon\Mvc\Model\Validator\InclusionIn,
    Phalcon\Mvc\Model\Validator\PresenceOf,
    Phalcon\Mvc\Model\Message,
    Phalcon\Mvc\Model\Validator\Uniqueness,
    Phalcon\Mvc\Model\Validator\Email as Email;

class Emailtemplate extends \Phalcon\Mvc\Model
{
    // Include any traits this model implements
    use StandardModel;
    
    /**
     *
     * @var integer
     */
    public $id;

    /**
     *
     * @var string
     */
    public $code;

    /**
     *
     * @var string
     */
    public $name;

    /**
     *
     * @var string
     */
    public $subject;

    /**
     *
     * @var string
     */
    public $from_name;

    /**
     *
     * @var string
     */
    public $from;

    /**
     *
     * @var string
     */
    public $is_html;

    /**
     *
     * @var string
     */
    public $message;

    /**
     *
     * @var string
     */
    public $modified_dtm;

    /**
     *
     * @var string
     */
    public $created_dtm;

    /**
     * Independent Column Mapping.
     */
    public function columnMap()
    {
        return array(
            'id' => 'id', 
            'code' => 'code', 
            'name' => 'name', 
            'subject' => 'subject', 
            'from_name' => 'from_name', 
            'from' => 'from', 
            'is_html' => 'is_html', 
            'message' => 'message', 
            'modified_dtm' => 'modified_dtm', 
            'created_dtm' => 'created_dtm'
        );
    }
    
    /**
    * Validations and business logic
    */
    public function validation()
    {
        $this->validate(new PresenceOf(array(
            'field' => 'code',
            'message' => 'The code field is required'
        ))); 
        
        $this->validate(new Uniqueness(
            array(
                "field"   => "code",
                "message" => "This template code has already been used by another template."
            )
        ));        
        
        $this->validate(new PresenceOf(array(
            'field' => 'name',
            'message' => 'The name field is required'
        )));  
        
        $this->validate(new PresenceOf(array(
            'field' => 'subject',
            'message' => 'The subject field is required'
        ))); 
        
        $this->validate(new PresenceOf(array(
            'field' => 'from_name',
            'message' => 'The from_name field is required'
        ))); 
        
        $this->validate(new PresenceOf(array(
            'field' => 'from',
            'message' => 'The from field is required'
        )));                              
        
        $this->validate(new Email(array(
            'field'    => 'from',
            'required' => true
        )));        
        
        if ($this->validationHasFailed() == true) {
            return false;
        }
        
        return true;
    }  
    
    /**
    * Init method is used to define legal ordering columns, 
    * order-by behaviour, pagination limits et cetera.
    * 
    * @param mixed $di
    */
    public function init($di, $config)
    {
        $this->allowedOrderColumns = array("EmailTemplate.id", "EmailTemplate.name", "EmailTemplate.code", "EmailTemplate.created_dtm");
        $this->defaultOrderBy = "EmailTemplate.id ASC";
        $this->itemsPerPage = 25;    
        $this->config = $config;    
    }  
    
    /**
    * The getList method, as defined by the standard model trait, will invoke this 
    * apply filters method. Here we can define which columns are searchable, and what 
    * the search condition is. For example =, LIKE, <, >, >= etc
    * 
    * @param array $filters The filters array
    * @param array $conditions The conditions array that is populated by this method.
    * @param mixed $bind
    */
    protected function applyFilters($filters, &$conditions = array(), &$bind = array()) 
    {               
        $this->addFilter("code", $filters, $conditions, $bind, "LIKE");
        $this->addFilter("name", $filters, $conditions, $bind, "LIKE");     
        $this->addFilter("subject", $filters, $conditions, $bind, "LIKE");
        $this->addFilter("created_dtm", $filters, $conditions, $bind, ">=");
    }
    
    /**
    * Sends an email using the currently loaded database template, merging values from the 
    * emailData array into the template, adding any attachments.
    * 
    * @param array $emailData An array of key/value pairs to merge into the template
    * @param array $recipients An array of the recipients to send the email to
    * @param array $cc An array of the recipients to CC the email to - use false if CC is not required.
    * @param array $bcc An array of the recipients to BCC the email to - use false if BCC is not required.
    * @param array $attachments An array of the attachments to attach to the email.  Use false if no attachments.
    */
    public function sendEmail($emailData, $recipients, $cc = false, $bcc = false, $attachments = false)
    {
        // Template ID must be a valid number
        if(!is_numeric($this->id)) {
            throw new Exception("EmailTemplate::sendEmailFromTemplate - Please load the template object first");
        }
        
        // Recipients MUST be defined.
        if((!is_array($recipients)) || (count($recipients) == 0)) {
            throw new Exception("Emailtemplate::send - No recipients defined");
        }
        
        // Replace the dynamic fields in the email template with the passed values.
        //$compiler = new \Phalcon\Mvc\View\Engine\Volt\Compiler();
        $emailMessage = $this->message;

        foreach($emailData as $key => $value) {
            $emailMessage = str_replace("{" . $key . "}", $value, $emailMessage);
        }
        
        // Create an instance of PHP mailer
        $objMailer = new PHPMailer();
        $objMailer->SetFrom($this->from, $this->from_name);
        $objMailer->Subject = $this->subject;
        $objMailer->Body = $emailMessage;
        
        // Set the HTML email attribute
        $objMailer->IsHTML($this->is_html);
        
        // Add Primary recipients
        foreach($recipients as $name => $address) {
            $objMailer->AddAddress($address, $name);
        }
        
        // Add Carbon Copy recipients
        if((is_array($cc)) && (count($cc) > 0)) {
            foreach($cc as $name => $address) {
                $objMailer->AddCC($address, $name);
            }            
        }
        
        // Add Blind Carbon Copy recipients
        if((is_array($bcc)) && (count($bcc) > 0)) {
            foreach($bcc as $name => $address) {
                $objMailer->AddBCC($address, $name);
            }            
        }
        
        // Add attachments
        if(is_array($attachments)) {
            foreach($attachments as $attachmentName => $attachmentPath) {
                if(file_exists($attachmentPath)) {
                    $objMailer->AddAttachment($attachmentPath, $attachmentName);
                }
            }
        }       
        
        // Send the email.
        return $objMailer->Send(); 
    }           
}
