<?php
use Phalcon\Mvc\Model\Validator\InclusionIn,
    Phalcon\Mvc\Model\Validator\PresenceOf,
    Phalcon\Mvc\Model\Message,
    Phalcon\Mvc\Model\Validator\Uniqueness;
    
class Persons extends \Phalcon\Mvc\Model
{
    // Include any traits this model implements
    use StandardModel;    
    public $name;
    public $phone;
    
    
    /**
    * The initialize method is called by the framework automatically
    * Declare foreign key relationships here
    * http://docs.phalconphp.com/en/latest/reference/models.html#relationships-between-models
    */
    public function initialize()
    {
        $this->belongsTo("mechanic_id", "Mechanics", "id", array(
            "foreignKey" => array("message" => "The mechanic_id does not exist on the Mechanics table")
        ));
    }    
    
    /**
    * Init method is used to define legal ordering columns, 
    * order-by behaviour, pagination limits et cetera.
    * 
    * @param mixed $di
    */
    public function init($di)
    {
        $this->allowedOrderColumns = array("Persons.id", "Persons.name", "Persons.created_dtm");
        $this->defaultOrderBy = "Persons.name ASC";
        $this->itemsPerPage = 2;        
    }    
    
    public function validation()
    {
        // Mechanic name must be unique
        $this->validate(new Uniqueness(
            array(
                "field"   => "name",
                "message" => "The person name must be unique"
            )
        ));
        
        // mechanic_id is required
        $this->validate(new PresenceOf(array(
            'field' => 'mechanic_id',
            'message' => 'The mechanic_id is required'
        )));         

       //Check if any messages have been produced
        if ($this->validationHasFailed() == true) {
            $errorStr = "";
            
            foreach ($this->getMessages() as $message) {
                $errorStr .= $message->getMessage();
                $errorStr .= ", Field: " . $message->getField();
                $errorStr .= ", Type: " . $message->getType();
                $errorStr .= ". \n";
            }            
            
            throw new Exception("Validation failed: $errorStr");
        }
    } 
    
    /**
    * The getList method, as defined by the standard model trait, will invoke this 
    * apply filters method. Here we can define which columns are searchable, and what 
    * the search condition is. For example =, LIIKE, <, >, >= etc
    * 
    * @param array $filters The filters array
    * @param array $conditions The conditions array that is populated by this method.
    * @param mixed $bind
    */
    protected function applyFilters($filters, &$conditions = array(), &$bind = array()) 
    {               
        $this->addFilter("name", $filters, $conditions, $bind, "LIKE");     
        $this->addFilter("created_dtm", $filters, $conditions, $bind, ">=");
    }
}