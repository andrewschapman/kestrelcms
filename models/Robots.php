<?php
use Phalcon\Mvc\Model\Validator\InclusionIn,
    Phalcon\Mvc\Model\Validator\PresenceOf,
    Phalcon\Mvc\Model\Message,
    Phalcon\Mvc\Model\Validator\Uniqueness;

class Robots extends \Phalcon\Mvc\Model
{
    // Include any traits this model implements
    use StandardModel;
    
    // Define model database columns
    // No need to define id, created_dtm, modified_dtm 
    // as it is in the StandardModel trait.    
    public $name;
    public $type;
    public $year;
    public $mechanic_id;
    
    /**
    * The initialize method is called by the framework automatically
    * Declare foreign key relationships here
    * http://docs.phalconphp.com/en/latest/reference/models.html#relationships-between-models
    */
    public function initialize()
    {
        $this->belongsTo("mechanic_id", "Mechanics", "id", array(
            "foreignKey" => array("message" => "The mechanic_id does not exist on the Mechanics table")
        ));
    }
    
    /**
    * Init method is used to define legal ordering columns, 
    * order-by behaviour, pagination limits et cetera.
    * 
    * @param mixed $di
    */
    public function init($di)
    {
        $this->allowedOrderColumns = array("Robots.id", "Robots.name", "Robots.created_dtm");
        $this->defaultOrderBy = "Robots.name ASC";
        $this->itemsPerPage = 2;        
    }  
    
    /**
    * The validation method is called by the framework automatically 
    * before a record is saved or inserted. Put validation rules here.
    * 
    */
    public function validation()
    {
        // Name is required and must be unique
        $this->validate(new PresenceOf(array(
            'field' => 'name',
            'message' => 'The name is required'
        )));   
        
        $this->validate(new Uniqueness(
            array(
                "field"   => "name",
                "message" => "The robot name must be unique"
            )
        ));             
        
        // Type is required must be: droid, mechanical or virtual
        $this->validate(new PresenceOf(array(
            'field' => 'type',
            'message' => 'The name is required'
        )));   
        
                
        $this->validate(new InclusionIn(
            array(
                "field"  => "type",
                "domain" => array("droid", "mechanical", "virtual")
            )
        ));

        //Year cannot be less than 1960
        $this->validate(new PresenceOf(array(
            'field' => 'year',
            'message' => 'The year is required'
        ))); 
                
        if ($this->year < 1960) {
            $this->appendMessage(new Message("The year cannot be less than 1960", "year", "Custom"));
        }
        
        // mechanic_id is required
        $this->validate(new PresenceOf(array(
            'field' => 'mechanic_id',
            'message' => 'The mechanic_id is required'
        )));         

        //Check if any messages have been produced
        if ($this->validationHasFailed() == true) {
            $errorStr = "";
            
            foreach ($this->getMessages() as $message) {
                $errorStr .= $message->getMessage();
                $errorStr .= ", Field: " . $message->getField();
                $errorStr .= ", Type: " . $message->getType();
                $errorStr .= ". \n";
            }            
            
            throw new Exception("Validation failed: $errorStr");
        }
    }
    
    /**
    * The getList method, as defined by the standard model trait, will invoke this 
    * apply filters method. Here we can define which columns are searchable, and what 
    * the search condition is. For example =, LIIKE, <, >, >= etc
    * 
    * @param array $filters The filters array
    * @param array $conditions The conditions array that is populated by this method.
    * @param mixed $bind
    */
    protected function applyFilters($filters, &$conditions = array(), &$bind = array()) 
    {               
        $this->addFilter("type", $filters, $conditions, $bind, "=");
        $this->addFilter("name", $filters, $conditions, $bind, "LIKE");     
        $this->addFilter("created_dtm", $filters, $conditions, $bind, ">=");
    }   
}