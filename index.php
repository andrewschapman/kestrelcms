<?php
try {
    /**
    * Read the configuration
    */
    $config = new Phalcon\Config\Adapter\Ini(__DIR__ . '/config/config.ini'); 

    // Use Loader() to autoload our models, plugins, libraries etc
    $loader = new \Phalcon\Loader();

    $loader->registerDirs(array(
        __DIR__ . $config->application->controllersDir,
        __DIR__ . $config->application->pluginsDir,
        __DIR__ . $config->application->libraryDir,
        __DIR__ . $config->application->modelsDir
    ))->register();
    

    // Register the libraries we want to use
    $loader->registerClasses(
        array(
            "StandardModel" => "library/StandardModel.php",
            //"KestrelSession" => "library/KestrelSession.php",
            "PHPMailer"     => "library/class.phpmailer.php",
            "POP3"          => "library/class.pop3.php",
            "SMTP"          => "library/class.smtp.php",
        )
    );    
        
    // Setup dependency injection
    $di = new \Phalcon\DI\FactoryDefault();

    //Set up the database service
    $di->set('db', function() use ($config) {
        return new \Phalcon\Db\Adapter\Pdo\Mysql(array(
            "host" => $config->database->host,
            "username" => $config->database->username,
            "password" => $config->database->password,
            "dbname" => $config->database->name,
            "charset" => "utf8"
        ));
    });
    
    //Registering the Models-Metadata
    $di->set('modelsMetadata', 'Phalcon\Mvc\Model\Metadata\Memory');    
    
    $di->set('modelsManager', function() {
        return new Phalcon\Mvc\Model\Manager();
    });  
    
    // Register Volt as template engine
    $di->set('view', function() {

        $view = new \Phalcon\Mvc\View();

        $view->setViewsDir(__DIR__ . $config->application->modelsDir);

        $view->registerEngines(array(
            ".volt" => 'Phalcon\Mvc\View\Engine\Volt'
        ));

        return $view;
    });      

    $app = new \Phalcon\Mvc\Micro($di);
    
    // Determine the route file that we need based on the URI.
    // E.g. if /api/user appears in the URL, we will include the "user" routes file 
    // in config/routes
    $objURI = new Kestrel\URI($config->application->baseSegment);
    $controllerName = $objURI->getSegment(1);
    
    $routePath = $config->application->absolutePath . "config/routes/" . $controllerName . ".php";
    if(!file_exists($routePath)) {
        throw new Exception("Invalid controller $routePath");
    }
    
    // Setup cache object
    if($config->application->cacheType == "memcached") {
        Kestrel\Cache::init($config->application->cacheHost, $config->application->cachePort, $config->application->cacheTimeout);
    }
    
    // Include the route declarations.
    require_once($routePath);

    $app->handle();
}
catch(\Phalcon\Exception $e) {
    echo "PhalconException: ", $e->getMessage();
} catch (PDOException $e){
    echo $e->getMessage();
} catch(Exception $e) {
    echo "General Exception: " . $e->getMessage();
}    