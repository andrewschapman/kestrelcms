<?php
namespace Kestrel;

class Cache
{
    private static $timeout = 3600;       // The cache timeout
    private static $host = "localhost";   // Memcached host
    private static $port = "11211";       // Memcached port
    private static $objCache = false;     // Set to a Phalcon cache object
    
    /**
    * Sets up the cache connection to memcached.
    * 
    * @param string $host The memcached host name
    * @param integer $port  The memcached server port
    * @param integer $timeout  The cache timeout value in seconds
    */
    public static function init($host = "localhost", $port = "11211", $timeout = 3600) {
        self::$host = $host;
        self::$port = $port;
        self::$timeout = $timeout;
        
        // Cache data for $timeout seconds
        $frontCache = new \Phalcon\Cache\Frontend\Data(array(
            "lifetime" => self::$timeout
        ));

        // Create the component that will cache "Data" to a "Memcached" backend
        // Memcached connection settings
        self::$objCache = new \Phalcon\Cache\Backend\Libmemcached($frontCache, array(
            "host" => self::$host,
            "port" => self::$port
        ));        
    }
    
    /**
    * Sets data into the cache
    * 
    * @param string $key The cache key to save the value for
    * @param mixed $value The cache value to store - Could be any kind of value.
    */
    public static function set($key, $value)
    {
        self::$objCache->save($key, $value);
    }
    
    /**
    * Gets an item from the cache
    * 
    * @param string $key The key to retrieve from the cache.
    * @returns The key value is the key is found, an empty string if not.
    */
    public static function get($key)
    {
        if (!self::$objCache->exists($key)) {
            return "";
        }
        
        return self::$objCache->get($key);
    }
    
    /**
    * Determines if a cache key exists or not.
    * 
    * @param string $key The key to check for the existence of
    * @returns True if key exists, false if not.
    */
    public static function exists($key)
    {
        return self::$objCache->exists($key);    
    }
    
    /**
    * Deletes an item from the cache
    * 
    * @param string $key The key to delete
    */
    public static function delete($key)
    {
        if (self::$objCache->exists($key)) {
            self::$objCache->delete($key);                
        }
    }
}