<?php
/**
* The URI class provides convenience methods for accessing and manipulating
* URIs
*/

namespace Kestrel;

class URI
{
    private $request;       // A Phalcon request object
    private $baseSegment;   // We skip anything BEFORE and INCLUDING the baseSegment when determinig what URI segments are available.
    private $segments;      // An array holding all the segment names AFTER the baseSegment
    private $numSegments;   // The number of segments in the URI after the baseSegment
    
    public function __construct($baseSegment = "")
    {
        $this->request = new \Phalcon\Http\Request();
        $this->segments = false;
        $this->baseSegment = strtolower($baseSegment);
        $this->numSegments = 0;
    }
    

    public function getSegment($segmentIndex)
    {
        if(!is_numeric($segmentIndex)) {
            throw new Exception("URI::getSegment Invalid segment - Not a number");
        }
        
        $segmentIndex = intval($segmentIndex);
        
        if($segmentIndex <= 0) {
            throw new Exception("URI::getSegment Invalid segment - segment index too low");
        }
        
        // If the segements array hasn't been created - lazy load that bitch
        if(!$this->segments) {
            $this->getSegments();
        }
        
        if($segmentIndex > $this->numSegments) {
            throw new Exception("URI::getSegment Invalid segment - segment index too high and doesn't exist");    
        }
        
        // Return the segment name
        return $this->segments[$segmentIndex - 1];
    } 
    
    /**
    * Gets the segments from the URI, exluding everything
    * before the baseSegment
    */
    private function getSegments()
    {
        if(!$this->segments) {
            
            if((empty($this->baseSegment)) || ($this->baseSegment == "/")) {
                $this->segments = explode("/", strtolower($this->request->getURI()));    
            } else {
                $this->segments = [];
                $temp = explode("/", strtolower($this->request->getURI()));
                $foundBaseSegment = false;
            
                foreach($temp as $segment) {
                    if(!$foundBaseSegment) {
                        if($segment == $this->baseSegment) {
                            $foundBaseSegment = true;    
                        }
                    } else {
                        $this->segments[] = $segment;    
                    }
                }
            }
            
            $this->numSegments = count($this->segments);
        }
    }           
}
