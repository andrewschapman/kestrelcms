<?php
trait StandardModel
{
    protected $defaultOrderBy;      // The default orderBy when returning a list
    protected $itemsPerPage;        // The default number of items per page
    public $allowedOrderColumns;    // An array containing the names of legal columns that may be sorted by
    protected $config;                // A reference to the app config object
    
    // Every database record should have these columns
    public $id;
    public $created_dtm;
    public $modified_dtm;
    
    
    /**
    * Gets a single item from the database from the table that the model operates on.
    * @param integer $id
    */
    public function get($id)
    {
        $item = $this->findFirst("id=" . $id);
        return $item;    
    }             
    
    public function beforeValidationOnCreate()
    {
        $this->created_dtm = date('Y-m-d H:i:s');
        $this->modified_dtm = date('Y-m-d H:i:s');
    }

    public function beforeValidationOnUpdate()
    {
        $this->modified_dtm = date('Y-m-d H:i:s');
    }
    

    /***
    * Loads a list of records from the database table, taking into account any filters specified in the database.
    * 
    * @param array $filters An associative array of any filters to apply
    * @param string $orderBy An order by statement.  If not provided, the models default order by will be used.
    * @param int $page If you want to load a limited set of the data for pagination, specify the current page number.  Pass 0 for no pagination.
    * @param int $totalRows An output param, if you're using pagination this will populate with the total number of rows.
    * @return array
    */
    public function getList($filters, $order="", $page = 0, &$totalRows = 0) {
        
        $this->applyFilters($filters, $conditions, $bind);

        // Ensure a sensible ordering statement has been set
        if((empty($order)) && (empty($this->defaultOrderBy))) {
            $this->app->error(new \Exception("Myndie/Model/Model::getList - OrderBy not set and no default order set either"));
        } else if((empty($order)) && (!empty($this->defaultOrderBy))) {
            $order = $this->defaultOrderBy;    
        }
        
        $params = array(
            "conditions" => $conditions,
            "bind" => $bind,
            "order" => $order        
        );
        

        // If a page number parameter was passed, give the results for the specified page.
        if($page > 0) {
            $totalRows = $this->count($params);
            
            
            $offset = 0;
            
            if($page > 1) {
                $offset = ($page - 1) * $this->itemsPerPage;
            }
            
            $params["offset"] = $offset;
            $params["limit"] = $this->itemsPerPage;    
        }        

        $result = $this->find($params);

        return $result;
    }
    
    /**
    * Takes into consideration the specified column and order direction
    * and confirms they are both valid and legal.  If not, exceptions
    * are thrown.  Otherwise the order by statement is returned ready for use.
    * 
    * @param string $orderCol The column we wish to order by
    * @param string $orderDir The desired order direction
    * @return string The order by result.
    */
    public function getOrderBy($orderCol, $orderDir)
    {
        // If either column or direction are empty, return nothing
        if((empty($orderCol)) || (empty($orderDir))) {
            return "";
        }
        
        // Ensure that the order value is legal and the order direction is legal
        if(!in_array($orderCol, $this->allowedOrderColumns)) {
            throw new Exception("You may not order by this column: $orderCol");
        }
        
        $orderDir = strtoupper($orderDir);
        if(($orderDir != "ASC") && ($orderDir != "DESC")) {
            throw new Exception("Invalid record ordering direction: $orderDir");
        }
        
        // Built the order by string and return it
        $order = $orderCol . " " . $orderDir;
        
        return $order;
    } 
    
    public function getListSQL($sql, $page = 0, &$totalRows = 0)
    {
        $query = new Phalcon\Mvc\Model\Query($sql, $this->di);    
        
        $result = $query->execute();
        
        return $result;
    }  
        
    
    public function addFilter($key, $filters, &$conditions = "", &$bind, $operator = "=")
    {
        // If the filter doesn't exist, nothing to do.
        if(!array_key_exists($key, $filters)) {
            return;    
        }
        
        // Get the filter value
        $value = $filters[$key];
        
        // If the value is empty then return.  Note, a 0 value returns true by the empty function
        // so we have to test for a valid numeric value to prevent this.
        if ((empty($value)) && (!is_numeric($value))) {
            return;                        
        }
        
        if(!empty($conditions)) {
            $conditions .= " AND ";
        }
        
        $conditions .= $key . " " . $operator . " :" . $key . ":";
        
        switch($operator) {
            case "LIKE":
                $bind[$key] = '%' . $filters[$key] . "%";            
                break; 
                
            default:
                $bind[$key] = $value;  
                break;               
        }
        
    }    
}