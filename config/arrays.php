<?php
/***************************************************
* Firewall Arrays
/***************************************************/

// Add any controllers where the ENTIRE controller and all its methods are open to the public here.
$kestrelPublicController = array(
    "country", "state"
);
  
// Add any method uris where the specific controller method is open to the public here
// Note, if the method takes additional parameters, e.g. /api/states/list/1, leave out the parameter
// so just add /api/ then the controller name, then the method name, e.g. /api/states/list
$kestrelPublicURI = array(
    "/api/user/login",      // Allow user logins publically
    "/api/user/register",    // Allow user registrations publically
    "/api/emailtemplate/sendtest"
);  

// If your entire controllers should be restircted to specific user roles, add them here
// e.g. "state" => array(MYNDIE_ROLE_ADMIN, MYNDIE_ROLE_MEMBER)  
$kestrelRestrictedControllers = array(
    "emailtemplate" => array($config->roles->admin)    // Only admin should have access to the email template controller
);

// Add any URIS here that should be restricted to particular login roles.
// e.g.  "/api/state/list" => array(MYNDIE_ROLE_ADMIN, MYNDIE_ROLE_MEMBER) 
// You can specify as many roles as you want with comma separated values.
// Note, if a URI is NOT in this array, as long as the user is logged in, they will be able to access it.
$kestrelRestrictedURIs = array(
    "/api/state/list" => array($config->roles->admin, $config->roles->member)    
);
