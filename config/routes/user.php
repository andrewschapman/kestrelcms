<?php
    // Retrieves all items
    $app->get('/api/user', function() use ($app, $di) {
        $objController = new UserController($di);
        $objController->getList();
    });  
    
    // Retrieves a single item based on ID
    $app->get('/api/user/{id:[0-9]+}', function($id) use ($app, $di) {
        $objController = new UserController($di);
        $objController->get($id);
    });    

    // Adds a new item
    $app->post('/api/user', function() use ($app, $di) {
        $objController = new UserController($di);
        $objController->save();
    });
    
    // Adds a new item
    $app->post('/api/user/register', function() use ($app, $di) {
        $objController = new UserController($di);
        $objController->register();
    });
    

    // Updates an item based on primary key
    $app->post('/api/user/{id:[0-9]+}', function($id) use($app, $di) {
        $objController = new UserController($di);
        $objController->save($id);
    });
    
    // Deletes items based on primary key, can be multiple comma separated
    $app->delete('/api/user/{id:[0-9,]+}', function($ids) use ($app, $di) {
        $objController = new UserController($di);
        $objController->delete($ids);
    });
    
    $app->post('/api/user/login', function() use($app, $di) {
        $objController = new UserController($di);
        $objController->login();
    });      